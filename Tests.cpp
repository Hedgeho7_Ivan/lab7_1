/*!
\file Tests.cpp
\brief ���� � assert �������

������ ���� �������� � ���� ����� � ������� ���������
*/

#include "StdAfx.h"
#include <cfixcc.h>

void decipherString (char *copyString);												// ������� ��� ���������� ������
void flipSymbolGroup (char *copyString, int kolvoGroups, int sizeLastGroup, int N);	// ������� ��� ��������������� ������ ��������
void preparationNameOfTheOutputFile (char* createOutputFile, char* expansion,char *argv[1]); // ������� ��� �������� �������� ��������� �����
int checkForInvalidCharacters (int currentString, char* string);					// ������� ��� �������� ������  �� ������� ������������ ���������� // ���������� ���� ������ ���� 0
int checkNSymbols (int currentString, char* textString);							// ������� ��� �������� ��������� �������� �����

int j = 0;

class Tests : public cfixcc::TestFixture
{
private:

public:
	void testFunctionToPrepareForFlipGroupsSymbols()
	{//������������ �������, ������� ��������� ������ �� ������� � ������ ������� ����� � ���������� �� ��������� ��������
		char *testText	= new char[30];
		char *help		= new char[30];

		strcpy(help,"");
		strcpy(testText,help);
		CFIX_LOG(L"Test 1 (Empty string). Parameters:\"\"; expected:\"\" (������� ������ ��������)");
		decipherString(testText);	
		CFIX_ASSERT_MESSAGE(*testText == NULL,L"Fail! Expected:\"\",returned:%d", testText);
		
		strcpy(help,"��������");
		strcpy(testText,help);
		strcpy(help,"�������");
		CFIX_LOG(L"Test 2 (Normal string). Parameters:\"��������\"; expected:\"�������\" (������� ������ ��������)");
		decipherString(testText);	
		CFIX_ASSERT_MESSAGE(*testText == *help,L"Fail! Expected:\"�������\",returned:%d", testText);

		strcpy(help,"������������_����");
		strcpy(testText,help);
		// ����� ����� ������� �������
		CFIX_LOG(L"Test 3 (In the string of a group with an even number of characters without a residual string). Parameters:\"������������_����\"; expected:�����������_����");
		decipherString(testText);
		CFIX_ASSERT_MESSAGE(*testText == *help,L"Fail! Expected:\"�����������_����\",returned:%s", testText);

		strcpy(help,"��_��������������_��������_�");
		strcpy(testText,help);
		strcpy(help,"������_��_��������_��������");
		CFIX_LOG(L"Test 4 (In the string of a group with an not even number of characters without a residual string). Parameters:\"��_��������������_��������_�\"; expected:������_��_��������_��������");
		decipherString(testText);
		CFIX_ASSERT_MESSAGE(*testText == *help,L"Fail! Expected:\"������_��_��������_��������\",returned:%s", testText);

		strcpy(help,"������_����������");
		strcpy(testText,help);
		strcpy(help,"�����������_����");
		CFIX_LOG(L"Test 5 (In the string of a group with an even number of characters with an even residual string). Parameters:\"������_����������\"; expected:�����������_����");
		decipherString(testText);
		CFIX_ASSERT_MESSAGE(*testText == *help,L"Fail! Expected:\"������_��_��������_��������\",returned:%s", testText);

		strcpy(help,"����������_���");
		strcpy(testText,help);
		strcpy(help,"�������_�����");
		CFIX_LOG(L"Test 6 (In the string of a group with an even number of characters with an odd residual string). Parameters:\"����������_���\"; expected:�������_�����");
		decipherString(testText);
		CFIX_ASSERT_MESSAGE(*testText == *help,L"Fail! Expected:\"�������_�����\",returned:%s", testText);

		strcpy(help,"��������������");
		strcpy(testText,help);
		strcpy(help,"�������������");
		CFIX_LOG(L"Test 7 (In the string of a group with an odd number of characters with an even residual string). Parameters:\"��������������\"; expected:�������������");
		decipherString(testText);
		CFIX_ASSERT_MESSAGE(*testText == *help,L"Fail! Expected:\"�������������\",returned:%s", testText);

		strcpy(help,"���������");
		strcpy(testText,help);
		strcpy(help,"��������");
		CFIX_LOG(L"Test 8 (In the string (without the \"N\" character) of a group with an odd number of characters with an odd residual string). Parameters:\"���������\"; expected:��������");
		decipherString(testText);
		CFIX_ASSERT_MESSAGE(*testText == *help,L"Fail! Expected:\"��������\",returned:%s", testText);

		delete testText;
		delete help;
	}

	void testFunctionForFlipGroupsSymbols()
	{//������������ �������, ������� �������������� ������ �������� �� N ��������
		char *testText	= new char[30];
		char *help		= new char[30];

		strcpy(help,"�");
		strcpy(testText,help);
		CFIX_LOG(L"Test 1 (Single symbol(residual group)). Parameters:\"�\",1,1,2; expected:�");
		flipSymbolGroup(testText,1,1,2);
		CFIX_ASSERT_MESSAGE(*testText == *help,L"Fail! Expected:\"�\",returned:%s", testText);

		strcpy(help,"����");
		strcpy(testText,help);
		strcpy(help,"����");
		CFIX_LOG(L"Test 2 (Several symbols(residual group)). Parameters:\"����\",1,4,5; expected:����");
		flipSymbolGroup(testText,1,4,5);
		CFIX_ASSERT_MESSAGE(*testText == *help,L"Fail! Expected:\"����\",returned:%s", testText);

		strcpy(help,"�����_��");
		strcpy(testText,help);
		strcpy(help,"����_���");
		CFIX_LOG(L"Test 3 (Normal groups (without residual group))). Parameters:\"�����_��\",4,0,2; expected:����_���");
		flipSymbolGroup(testText,4,0,2);
		CFIX_ASSERT_MESSAGE(*testText == *help,L"Fail! Expected:\"����_���\",returned:%s", testText);

		strcpy(help,"���_����");
		strcpy(testText,help);
		strcpy(help,"����_���");
		CFIX_LOG(L"Test 4 (Normal group (without residual group))). Parameters:\"�����_��\",1,0,8; expected:����_���");
		flipSymbolGroup(testText,1,0,8);
		CFIX_ASSERT_MESSAGE(*testText == *help,L"Fail! Expected:\"����_���\",returned:%s", testText);

		strcpy(help,"����_���");
		strcpy(testText,help);
		strcpy(help,"����_���");
		CFIX_LOG(L"Test 5 (Normal groups with residual group). Parameters:\"����_���\",3,2,3; expected:����_���");
		flipSymbolGroup(testText,3,2,3);
		CFIX_ASSERT_MESSAGE(*testText == *help,L"Fail! Expected:\"����_���\",returned:%s", testText);

		strcpy(help,"_�������");
		strcpy(testText,help);
		strcpy(help,"����_���");
		CFIX_LOG(L"Test 6 (Normal group with residual group). Parameters:\"_�������\",2,3,5; expected:����_���");
		flipSymbolGroup(testText,2,3,5);
		CFIX_ASSERT_MESSAGE(*testText == *help,L"Fail! Expected:\"����_���\",returned:%s", testText);
		
		strcpy(help,"_�������");
		strcpy(testText,help);
		strcpy(help,"����_���");
		CFIX_LOG(L"Test 6 (Normal group with residual group). Parameters:\"_�������\",2,3,5; expected:����_���");
		flipSymbolGroup(testText,2,3,5);
		CFIX_ASSERT_MESSAGE(*testText == *help,L"Fail! Expected:\"����_���\",returned:%s", testText);


		delete testText;
		delete help;
	}

	void testPreparationNamesForTheOutputFile()
	{// ������������ �������, ������� ��������������� �������� ��� ��������� �����
		char *testText	= new char[40];
		char *help		= new char[40];
		char *extension = new char[40];
		char **nameFile	= new char*[2];
			nameFile[1] = new char[40];

		strcpy(nameFile[1], "text.txt");
		strcpy(help, "text_deciphered.txt");
		CFIX_LOG(L"Test 1 The text is in the same directory. Parameters:\"test.txt\"; expected:test_deciphered.txt");
		preparationNameOfTheOutputFile (testText, extension, nameFile);
		CFIX_ASSERT_MESSAGE(*testText == *help,L"Fail! Expected:\"test_deciphered.txt\",returned:%s", testText);

		strcpy(nameFile[1], "D:\\test\\secretCode.txt");
		strcpy(help, "secretCode_deciphered.txt");
		CFIX_LOG(L"Test 2 The text is in a different directory. Parameters:\"D:\test\secretCode.txt\"; expected:secretCode_deciphered.txt");
		preparationNameOfTheOutputFile (testText, extension, nameFile);
		CFIX_ASSERT_MESSAGE(*testText == *help,L"Fail! Expected:\"secretCode_deciphered.txt\",returned:%s", testText);

		strcpy(nameFile[1], "C:\\test\program\\treeHouse.txt");
		strcpy(help, "treeHouse_deciphered.txt");
		CFIX_LOG(L"Test 2 The text is in a different directory. Parameters:\"C:\test\program\treeHouse.txt\"; expected:treeHouse_deciphered.txt");
		preparationNameOfTheOutputFile (testText, extension, nameFile);
		CFIX_ASSERT_MESSAGE(*testText == *help,L"Fail! Expected:\"treeHouse_deciphered.txt\",returned:%s", testText);

		delete testText;
		delete help;
		delete extension;
		delete []nameFile;
	}

	void testFunctionCorrectnessSymbolsInText()
	{// ������������ �������, ������� ��������� ������������ �������� � ������
		char *testText	= new char[13];

		strcpy(testText,"����_����");
		CFIX_LOG(L"Test 1 (Normal string). Parameters:1,\"����_����\"; expected:0");
		CFIX_ASSERT_MESSAGE(checkForInvalidCharacters (1, testText) == 0,L"Fail! Expected:\"0\",returned:%d", checkForInvalidCharacters (1, testText));

		strcpy(testText,"");
		CFIX_LOG(L"Test 2 (Empty string). Parameters:1,\"\"; expected:0");
		CFIX_ASSERT_MESSAGE(checkForInvalidCharacters (1, testText) == 0,L"Fail! Expected:\"0\",returned:%d", checkForInvalidCharacters (1, testText));

		strcpy(testText,"g����_����");
		CFIX_LOG(L"Test 3 (Whong string(in beggining)). Parameters:1,\"g����_����\"; expected:4");
		CFIX_ASSERT_MESSAGE(checkForInvalidCharacters (2, testText) == 4,L"Fail! Expected:\"4\",returned:%d", checkForInvalidCharacters (1, testText));

		strcpy(testText,"����g_����");
		CFIX_LOG(L"Test 4 (Whong string(in middle)). Parameters:1,\"����g_����\"; expected:4");
		CFIX_ASSERT_MESSAGE(checkForInvalidCharacters (1, testText) == 4,L"Fail! Expected:\"4\",returned:%d", checkForInvalidCharacters (1, testText));

		strcpy(testText,"����_����g");
		CFIX_LOG(L"Test 5 (Whong string(in middle)). Parameters:1,\"����_����g\"; expected:4");
		CFIX_ASSERT_MESSAGE(checkForInvalidCharacters (1, testText) == 4,L"Fail! Expected:\"4\",returned:%d", checkForInvalidCharacters (1, testText));

		strcpy(testText,"��g��_����g");
		CFIX_LOG(L"Test 6 (Whong string(several)). Parameters:1,\"��g��_����g\"; expected:4");
		CFIX_ASSERT_MESSAGE(checkForInvalidCharacters (1, testText) == 4,L"Fail! Expected:\"4\",returned:%d", checkForInvalidCharacters (1, testText));

		strcpy(testText,"abcde");
		CFIX_LOG(L"Test 7 (Whong string(full string)). Parameters:1,\"abcde\"; expected:4");
		CFIX_ASSERT_MESSAGE(checkForInvalidCharacters (1, testText) == 4,L"Fail! Expected:\"4\",returned:%d", checkForInvalidCharacters (1, testText));

		delete testText;
	}

	void testFunctionCorrectnessNSymbol()
	{// ������������ �������, ������� ��������� ������������ N �������
		char *testText	= new char[11];

		strcpy(testText,"����_����");
		CFIX_LOG(L"Test 1 (Normal string). Parameters:1,\"����_����\"; expected:0");
		CFIX_ASSERT_MESSAGE(checkNSymbols (1, testText) == 0,L"Fail! Expected:\"0\",returned:%d", checkNSymbols (1, testText));

		//strcpy(testText,"");																										// commit1
		//CFIX_LOG(L"Test 2 (Empty string). Parameters:1,\"\"; expected:0");														// commit1
		//CFIX_ASSERT_MESSAGE(checkNSymbols (1, testText) == 0,L"Fail! Expected:\"0\",returned:%d", checkNSymbols (1, testText));	// commit1

		strcpy(testText,"����_���7");
		CFIX_LOG(L"Test 1 (Wrong \"N\"). Parameters:1,\"����_����\"; expected:5");
		CFIX_ASSERT_MESSAGE(checkNSymbols (1, testText) == 5,L"Fail! Expected:\"5\",returned:%d", checkNSymbols (1, testText));

		delete testText;
	}
};

CFIXCC_BEGIN_CLASS(Tests)
	CFIXCC_METHOD(testFunctionToPrepareForFlipGroupsSymbols)
	CFIXCC_METHOD(testFunctionForFlipGroupsSymbols)
	CFIXCC_METHOD(testPreparationNamesForTheOutputFile)
	CFIXCC_METHOD(testFunctionCorrectnessSymbolsInText)
	CFIXCC_METHOD(testFunctionCorrectnessNSymbol)
CFIXCC_END_CLASS()
