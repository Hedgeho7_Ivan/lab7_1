/*!
\file TTDTT_Function.h
\brief ������������ ���� � ��������� �������

������ ���� �������� � ���� �������
*/
#include "stdafx.h"
#include "string.h"
#include <clocale>
#include <iostream>
#include <fstream>

/* ������� ��� ���������� ������*/
void decipherString (char *copyString);
/* ������� ��� ��������������� ������ ��������*/
void flipSymbolGroup (char *copyString, int kolvoGroups, int sizeLastGroup, int N);	
/* ������� ��� �������� �������� ��������� �����*/
void preparationNameOfTheOutputFile (char* createOutputFile, char* expansion,char *argv[1]);
/* ������� ��� ������ ����� � ����*/
void output (int kolvoFlag, int kolvoStrok, char* createOutputFile, char* copyString);		
/* ������� ��� �������� ������  �� ������� ������������ ����������*/
int checkForInvalidCharacters (int currentString, char* string);
/* ������� ��� �������� ��������� �������� �����*/							
int checkNSymbols (int currentString, char* textString);									

using namespace std;

/*!\brief ������� ��� ���������� ������
*\param [in,out] copyString � ������ ��� ��������������

������� ���������� "N" ������, ������� ���-�� �����, ������� ������ ���������� ������ � �������� ������� �� ��������� ������
*/
void decipherString (char *copyString)						// ������� ��� ����������� ������
{
	char alphabet[] = "��������������������������������";	// ������� ��� �������� ���������� ������� ������
	if (strlen(copyString) != 0)
	{														// ���� ������ �����, �� ������ ������
		int symbolN = -1;									// ������ ����� � ��������
		char lastSymbol = copyString[strlen(copyString)-1];	// ��������� ������ � ������

		for (int r = 0; r<strlen(alphabet); r++)			// ���� ��������� ������
		{
			if (alphabet[r] == lastSymbol)					// ���� ��������� ������ ����� ������� �� ��������
			{
				symbolN = r;								// �� ���������� ����� ������� � ��������
				r = strlen(alphabet);
			}
		}
		copyString[strlen(copyString)-1] = '\0';			// ������� ��������� ������ ������
		int N = symbolN + 2;								// ������� ������ �����
		int kolvoGroups = strlen(copyString)/N;				// ���������� ���-�� ������ �����
		int sizeLastGroup = strlen(copyString)%N;			// ���������, ���� �� ��������� ��������� ������
		if (sizeLastGroup > 0)								// ���� ����
			kolvoGroups++;									// ����������� ���-�� ����� �� 1
		flipSymbolGroup (copyString, kolvoGroups, sizeLastGroup, N); // �������������� ������											// �������� ���������, � ���������� ������
	}
}

/*!\brief ������� ��� ��������������� ����� ��������
*\param [in,out] copyString � ������ ��� ��������������
*\param [in] kolvoGroups � ���-�� ����� � ������ (������� ���������� ������)
*\param [in] sizeLastGroup � ������ ���������� ������
*\param [in] N � ������ ����� � ��������� (����� ���������� ������)

������� �������������� ������ �������� � ������
*/
void flipSymbolGroup (char *copyString, int kolvoGroups, int sizeLastGroup, int N)	// ������� ��� ��������������� ����� ��������
{
	char flip;																	// ���������� ��� ������������ ��������														//
		for (int i = 0; i < (kolvoGroups); i++)									// ��� ������ ������
		{
			if (i < (kolvoGroups - 1))											// ���� ��� �� ���������
			{
				for (int j = 0; j < (N/3); j++)									// ����������� ��������� ��������
				{
					flip = copyString[j+(i*N)];
					copyString[j+(i*N)] = copyString[N-1+(i*N)-j];
					copyString[N-1+(i*N)-j] = flip;
				}
			}
			else if (sizeLastGroup > 1)											// ��� ��������� ������
			for (int j = 0; j < sizeLastGroup/3; j++)							// ����������� ��� �� ��������
			{																	// �������� ������������ � ����� ������
				flip = copyString[j+(i*N)];										// ����������� ��������� ��������
				copyString[j+(i*N)] = copyString[strlen(copyString)-1-j];
				copyString[strlen(copyString)-1-j] = flip;
			}
			else if (sizeLastGroup < 0)
				for (int j = 0; j < (N/3); j++)									
				{
					flip = copyString[j+(i*N)];
					copyString[j+(i*N)] = copyString[N-1+(i*N)-j];
					copyString[N-1+(i*N)-j] = flip;
				}
			else
				for (int j = 0; j < (N/2); j++)									
				{
					flip = 404;
				}
		}
}

/*!\brief ������� ��� ���������� �������� ��������� �����
*\param [in,out] createOutputFile - ������ ������
*\param [in,out] expansion � ���������� �����
*\param [in] argv[1] � ��������(����) ����� � �������

������� ����������� ��������(����) ����� � �������� ��������� �����
*/
void preparationNameOfTheOutputFile (char* createOutputFile, char* expansion, char* argv[1]) // ������� ��� �������� �������� ��������� �����
{
	char flip; //���������� ��� ������������ ��������

	strcpy(createOutputFile, argv[1]);					// �������� �������� ����� � ������
	strcpy(expansion,createOutputFile);					// �������� �������� ����� �� ������ ������
	_strrev(expansion);									// �������������� ������ ������
	expansion[strchr(expansion,'.')-expansion] = '\0';	// ��������� ������ ������������ ���������� ������ ������
	for(int y = 0; y < strlen(expansion)/2; y++)		// �������������� ����������
	{
		flip = expansion[y];
		expansion[y] = expansion[strlen(expansion)-1-y];
		expansion[strlen(expansion)-1-y] = flip;
	}
	createOutputFile[strrchr(createOutputFile,'.')-createOutputFile] = '\0'; // �������� ���������� � ������ � ������ ������
	if (strchr(createOutputFile,'\\') != NULL)
	{																		 // ���� ����������� ���� �����, �� ������� ����
		int j = strrchr(createOutputFile,'\\')-createOutputFile;
		for (int i = 0; i < (strlen(createOutputFile)-j-1); i++)
			createOutputFile[i] = createOutputFile[j+i+1];
		createOutputFile[strlen(createOutputFile)-j-1] = '\0';
	}
	strcat(createOutputFile, "_deciphered.");	// ���������� � ������ ����� ������� "_deciphered."
	strcat(createOutputFile, expansion);		// ���������� ����������
}

/*!\brief ������� ��� ������ ����� � ����
*\param [in] kolvoFlag � ������� ������
*\param [in] kolvoStrok � ���-�� ����� � ������
*\param [in] createOutputFile � �������� ��������� �����
*\param [in] copyString � �������������� ������

������� ���������� �������������� ������ � ����
*/
void output (int kolvoFlag, int kolvoStrok, char* createOutputFile, char* copyString)
{
	ofstream finalText;
	if (kolvoFlag == 1)					// ���� ������������ ������ ������
	finalText.open(createOutputFile);	// �� ���, �������� ���������� ������
	else finalText.open((createOutputFile),std::ios::app); // ����� ������ ���������� � �����
	if (kolvoFlag != kolvoStrok)		// ���� ������������ �� ��������� ������
	finalText<<copyString<<endl;		// �� � ����� ������ ��������� ���� ��������
	else {finalText<<copyString;		// �����, ��� ������, ������� ��� ������
			finalText.close();}			// � �������� �����
}

/*!\brief ������� ��� �������� ������  �� ������� ������������ �������� � ������
*\param [in] currentString � ����� ��������������� ������
*\param [in] string � ������ ������
*\return 0 � ��� ������, 4 � � ������ ������������ ������������ �������

������� �������� �� ������ �������� ������� � ����������� ���������
*/
int checkForInvalidCharacters (int currentString, char* string)
{
	int i;
	char signs[] = "�������������������������������������Ũ��������������������������0123456789,.?!:;-� _~^�%+=*<>()[]{}\"\'\\\/";
	// ������ � ���������� ��������� � 0 �� 32 �� � �� � � 33 �� 65 �� � �� � � 66 �� 75 �� 0 �� 9 ����� ��������� �������

	if((i = strspn(string,signs))<strlen(string))	// ���� ���������� ������������ ������, ��������� ��� �� ����������
		{
			printf("%s %c %s %d %s %d %s %c","������������ ������",string[i],"�",currentString,"������",(i+1),"�������",'\n');
			return 4;								// ���������, ��� ��������� ������
		}
	else
	{ 
		return 0;
	}
}

/*!\brief ������� ��� �������� ���������� ������� ������ �� ������������
*\param [in] currentString � ����� ��������������� ������
*\param [in] textString � ������ ������
*\return 0 � ��� ������, 5 � N � ������������ ������

������� ��������� ��������� ������ ������ � ����������� ���������
*/
int checkNSymbols (int currentString, char* textString) // ������� ��� �������� ��������� �������� �����
{
	char alphabet[] = "��������������������������������";	// ������� ��� �������� ���������� ������� ������
	if (strlen(textString) != 0)
	{														// ���� ������ �����, �� ��������� ������
		int symbolN = -1;									// ������ ����� � ��������
		char lastSymbol = textString[strlen(textString)-1];	// ��������� ������ � ������

		for (int r = 0; r<strlen(alphabet); r++)			// �������� �� ��������� ������ � ������ ����������
		{
			if (alphabet[r] == lastSymbol)
			{
				symbolN = r;								// �� ������ N �������� �������� ����� �� ��������
				r = strlen(alphabet);
			}
		}

		if (symbolN < 0)									// ���� ������ �� ��������
		{
			printf("����������� ������, ������������ ������ ������ %d %s %d %s %c",strlen(textString),"������,",currentString,"������",'\n');
			return 5;										// ���������� ��������� �� ������
		}
		else return 0;
	}
	else return 0;
}